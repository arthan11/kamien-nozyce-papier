# -*- coding: utf-8 -*-
import sys

KAMIEN = 0
NOZYCE = 1
PAPIER = 2

class Menu(object):
    pass

class Gracz(object):
    def __init__(self, nr):
        self.nr = nr

class Game(object):
    def __init__(self):
        self.max_graczy = 2
        self.gracze = []
        self.menu = Menu()

    def dodaj_gracza(self, nr):
        gracz = Gracz(nr)
        self.gracze.append(gracz)
        return gracz

    def usun_gracza(self, gracz):
        self.gracze.remove(gracz)

    def czy_istnieje_gracz(self, nr):
        for gracz in self.gracze:
            if gracz.nr == nr:
                return True
        return False

if __name__ == "__main__":
    game = Game()
    gracz = game.dodaj_gracza()
    print ( len(game.gracze) )

    game.usun_gracza(gracz)
    print ( len(game.gracze) )