# -*- coding: utf-8 -*-
import sys
import tornado.ioloop
import tornado.web
import tornado.websocket as websocket
import json
from game_core import Game

cmds_test = ['test']
cmds_menu = ['menu']

connections = []

def GlobalMsg(msg):
    json_cmd = json.dumps(msg)
    print ( 'global msg: "{}"'.format(json_cmd) )
    #print 'global msg: "%s"' % msg['cmd']
    for con in connections:
        try:
            con.write_message(json_cmd)
        except:
            pass

class MainHandler(websocket.WebSocketHandler):
    def __init__(self, application, request):
        self.cmds = cmds_menu
        self.gracz = None
        return super(MainHandler, self).__init__(application, request)

    def check_origin(self, origin):
        return True
    
    def Msg(self, msg):
        json_cmd = json.dumps(msg)
        print ( 'msg to %d: "%s"'.format(self.connId(), json_cmd) )
        #print 'msg to %d: "%s"' % (self.connId(), msg['cmd'])
        self.write_message(json_cmd)

    def OnClientCmd(self, cmd):
        #print cmd, self.connId()
        #    if cmd['cmd'] == 'reset':
        self.Msg(cmd)

    def connId(self):
        return connections.index(self)

    def open(self):
        connections.append(self)
        print ( "{} connected".format(self.connId()) )
        self.Msg({'cmd': 'pokaz_menu'})
        for con in connections:
            if con != self:
                if con.gracz:
                    self.Msg({'cmd': 'menu_hide', 'id': 'option_'+ str(con.gracz.nr)})


    def test(self):
        GlobalMsg({'cmd': 'test'})
        #self.Msg({'cmd': 'show_msg', 'msg': 'Nie udane logowanie!'})
        #print 'test'

    def on_message(self, message):
        print ( self.request.remote_ip )
        try:
            msg = json.loads(message)
        except:
            return

        if 'cmd' in msg:
            cmd = msg['cmd']
        else:
            cmd = ''
        if 'params' in msg:
            params = msg['params']
        else:
            params = []
        print ( cmd )

        '''
        for con in connections:
            if con == self:
               con.write_message('ty:' + message)
            else:
               con.write_message('con %d:' % self.connId() +  message)
        '''
        if cmd in self.cmds:
            if len(params) > 0:
                getattr(self, cmd)(*params)
            else:
                getattr(self, cmd)()
        else:
            msg = 'Niedozwolona operacja!\nMożliwe operacje to:'
            for cmd in self.cmds:
                msg = msg + '\n' + cmd
            self.Msg({'cmd':'show_msg', 'msg': msg})

    def on_close(self):
        conn_id = self.connId()
        if self.gracz:
            game.usun_gracza(self.gracz)
        connections.remove(self)
        print ("{} disconnected".format(conn_id) )

    def menu(self, option_id):
        print ('wybrano z menu: ' + option_id)

        if option_id == 'option_1':
            wybrany_gracz = 1
        elif option_id == 'option_2':
            wybrany_gracz = 2

        len_gracze = len(game.gracze)
        if (len_gracze < game.max_graczy) and not game.czy_istnieje_gracz(wybrany_gracz):
            self.gracz = game.dodaj_gracza(wybrany_gracz)
            for con in connections:
                if con != self:
                    con.Msg({'cmd': 'menu_hide', 'id': option_id})
                else:
                    con.Msg({'cmd': 'menu_click', 'id': option_id})

        '''
        elif option_id == 'option_3':
            if (len_gracze == 0):
                game.dodaj_gracza(1)
                game.dodaj_gracza(2)
        '''

application = tornado.web.Application([(r"/", MainHandler),])

if __name__ == "__main__":
    game = Game()
    try:
        application.listen(1337)
        print ('Serwer włączony.' )
    except:
        print ( 'Nie można właczyć serwera. Czyżby port był już w użyciu?' )
        sys.exit();
    tornado.ioloop.IOLoop.instance().start()
    tornado.ioloop.IOLoop.instance().stop()
