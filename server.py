import http.server
import socketserver

PORT = 80

Handler = http.server.SimpleHTTPRequestHandler

httpd = socketserver.TCPServer(("", PORT), Handler)

print ( 'serving at port {}'.format(PORT) )
httpd.serve_forever()