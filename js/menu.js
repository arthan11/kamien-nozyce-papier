//menu = new Menu($("#container"));
/*
  menu.add('GRACZ 1', 'img/joystick.png');
  menu.add('GRACZ 2', '', 'img/joystick.png');
  menu.add('GRACZ 1 &amp; 2', 'img/joystick.png', 'img/joystick.png');
  menu.add('PODGLĄD', 'img/monitor.png');
  menu.show_all();
  menu.show(2);
  menu.click(2);
  menu.hide(2);
*/

function Menu(container) {
  this.container = container;

  this.add = function(txt, img1, img2) {
    options_count = $('#' + this.container.attr('id') + ' .option').length;
    this.container.append(
      '<div class="option" id="option_' + (options_count + 1) + '">' +
      '  <div class="option_img_1"></div>' +
      '  <div class="option_text">' + txt + '</div>' +
      '  <div class="option_img_2"></div>' +
      '</div>');
    div_img1 = $('#option_' + (options_count + 1) + ' .option_img_1');
    div_img2 = $('#option_' + (options_count + 1) + ' .option_img_2');

    if ((typeof(img1) != 'undefined') && (img1 != '')) {
      div_img1.append('<img src="' + img1 + '">');
    };
    if ((typeof(img2) != 'undefined') && (img2 != '')) {
      div_img2.append('<img src="' + img2 + '">');
    };

    $('#option_'+ (options_count + 1)).bind('click', function() {
      id = $(this).attr('id');
      //alert(id);
      socket.doSend({'cmd': 'menu', 'params': [id]});
    });

  };

  this.show_all = function() {
    $(".option").velocity("transition.perspectiveDownIn", { stagger: 250 })
  };

  this.hide = function(id) {
    $('#' + id).velocity("transition.whirlOut");
  };

  this.show = function(id) {
    $('#' + id).velocity("transition.whirlIn");
  };

  this.click = function(id) {
    $('#' + id).velocity("callout.flash", {duration: 300});
    $('.option').velocity("transition.whirlOut", {duration: 1000, complete: this.clear});
  };

  this.clear = function(container) {
    $('.option').remove();
  };

};