var choices = null;

function Choices() {
  this.chose = function(choice) {
    choices.hide();
    alert(choice);
  };

  this.chose_1 = function() {
    choices.chose(1);
  };

  this.chose_2 = function() {
    choices.chose(2);
  };

  this.chose_3 = function() {
    choices.chose(3);
  };

  this.show = function() {
    $('.choices').css('display', 'block');
    $('.choices').velocity("transition.shrinkIn");
    $(document).on('keydown', null, '1', this.chose_1);
    $(document).on('keydown', null, '2', this.chose_2);
    $(document).on('keydown', null, '3', this.chose_3);
  };

  this.hide = function() {
    $(document).off('keydown', this.chose_1);
    $(document).off('keydown', this.chose_2);
    $(document).off('keydown', this.chose_3);
    $('.choices').velocity("transition.shrinkOut");
    $('.choices').css('display', 'none');
  };

  this.assign_click = function() {
    $('#ch_0').bind('click', choices.chose_1);
    $('#ch_1').bind('click', choices.chose_2);
    $('#ch_2').bind('click', choices.chose_3);
  };

};

function pokaz_menu() {
  menu.add('GRACZ 1', 'img/joystick.png');
  menu.add('GRACZ 2', '', 'img/joystick.png');
  //menu.add('GRACZ 1 &amp; 2', 'img/joystick.png', 'img/joystick.png');
  //menu.add('PODGLĄD', 'img/monitor.png');
  menu.show_all();

};

socket.onMessage = function (evt) {
  data = JSON.parse(evt.data)
  if (data.cmd == 'pokaz_menu') {
    pokaz_menu();
  } else if (data.cmd == 'menu_hide') {
      menu.hide(data.id);
  } else if (data.cmd == 'menu_click') {
      menu.click(data.id);
  };
};


var init = function() {
  loader = new Loader($("#container"));
  menu = new Menu($("#container"));
  gracze_vs = new GraczeVs();
  choices = new Choices();
  choices.assign_click();



  //choices.show();


  /*
  $('#option_1').bind('click', function() {
    menu.hide(2);
    //$(this).velocity("transition.whirlOut", { stagger: 250 })
  });
  */


};

window.addEventListener("DOMContentLoaded", init, false);