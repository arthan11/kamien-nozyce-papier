var wsUri = "ws://127.0.0.1:1337",
    socket = new Socket('output');

function Socket(outputId) {
  this.output = document.getElementById(outputId);
  this.websocket = new WebSocket(wsUri);
  this.websocket.onopen = function(evt) {socket.onOpen(evt)};
  this.websocket.onclose = function(evt) {socket.onClose(evt)};
  this.websocket.onmessage = function(evt) {socket.onMessage(evt)};
  this.websocket.onerror = function(evt) {socket.onError(evt)};

  this.onOpen = function (evt) {
    this.writeToScreen("CONNECTED");
  };

  this.onClose = function (evt) {
    this.writeToScreen("DISCONNECTED");
  };

  this.onMessage = function (evt) {
    this.writeToScreen('<span style="color: blue;">RESPONSE: ' + evt.data+'</span>');
    //websocket.close();
  };

  this.onError = function (evt) {
    this.writeToScreen('<span style="color: red;">ERROR:</span> ' + evt.data);
  };

  this.doSend = function (message) {
    //this.writeToScreen("SENT: " + message);
    json_msg = JSON.stringify(message, null, 2);
    this.websocket.send(json_msg);
  };

  this.writeToScreen = function (message) {
    //if (typeof(this.output) != 'undefined') {
    if (this.output != null) {
      var pre = document.createElement("p");
      pre.style.wordWrap = "break-word";
      pre.innerHTML = message;
      this.output.appendChild(pre);
      //this.output.innerHTML = message;
    };
  };
}

/*
window.addEventListener("load", init, false);
window.addEventListener("touchstart", touchStart, false);
window.addEventListener("touchmove", touchMove, false);
window.addEventListener("touchend", touchEnd, false);
*/