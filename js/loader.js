//loader = new Loader($("#container"));
//loader.show('PROSZĘ CZEKAĆ ...');
//loader.hide();

function Loader(container) {
  this.container = container;

  this.show = function(txt) {
    this.container.append(
      '<div class="loader">' +
      '  <div class="circle"></div>' +
      '  <div class="circle1"></div>' +
      '  <div class="text">' + txt + '</div>' +
      '</div>');
    $(".loader").velocity("transition.bounceIn", { duration: 1000 })
  };

  this.hide = function(txt) {
    $(".loader").velocity("transition.shrinkOut", {
      duration: 1000,
      complete: function() {
        $(".loader").remove();
      }
    });
  };
};
