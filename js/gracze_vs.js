  /*
  gracze_vs.show();
  gracze_vs.show_choices();
  gracze_vs.set_choice(1, -1);
  gracze_vs.set_choice(2, -1);
  */
  //gracze_vs.hide_choices();
  //gracze_vs.hide();

function GraczeVs() {
  this.show = function() {
    $('.gracze_vs').css('display', 'inline-block');
    $('.gracze_vs').velocity("transition.shrinkIn", {display: "inline-block"});
  };

  this.hide = function() {
    $('.gracze_vs').velocity("transition.shrinkOut", {display: "inline-block"});
    $('.gracze_vs').css('display', 'none');
  };

  this.show_choices = function() {
    $('.gracz_wybor').css('visibility', 'visible');
    $('.gracz_wybor').velocity("transition.bounceIn");
  };

  this.hide_choices = function() {
    $('.gracz_wybor').velocity("transition.bounceOut");
  };

  this.set_choice = function(gamer, choice) {
    if (choice == -1) {
      $('.gracz' + gamer + ' .graczy_wybor_txt').html('CZEKAM ...');
      $('.gracz' + gamer + ' .gracz_img img').addClass("hidden");
    } else if (choice == 0) {
      $('.gracz' + gamer + ' .graczy_wybor_txt').html('KAMIEŃ');
      $('.gracz' + gamer + ' .gracz_img img').attr('src', 'img/kamien.png');
      $('.gracz' + gamer + ' .gracz_img img').removeClass("hidden");
    } else if (choice == 1) {
      $('.gracz' + gamer + ' .graczy_wybor_txt').html('NOŻYCE');
      $('.gracz' + gamer + ' .gracz_img img').attr('src', 'img/nozyce.png');
      $('.gracz' + gamer + ' .gracz_img img').removeClass("hidden");
    } else if (choice == 2) {
      $('.gracz' + gamer + ' .graczy_wybor_txt').html('PAPIER');
      $('.gracz' + gamer + ' .gracz_img img').attr('src', 'img/papier.png');
      $('.gracz' + gamer + ' .gracz_img img').removeClass("hidden");
    } else {
      $('.gracz' + gamer + ' .graczy_wybor_txt').html('GOTOWY');
      $('.gracz' + gamer + ' .gracz_img img').addClass("hidden");
    };
  };

};